from numpy import NaN
import pandas as pd
from Move import Move

# global dataframe var for pokemon info
global df_pokemon
df_pokemon = pd.read_csv("data/pokemon.csv")


class Pokemon():
    # collect the pandas dataframe of pokemon here

    # maximum moves constant
    MAX_MOVES = 4

    def __init__(self, pokemon_name: str, nickname: str) -> None:
        # have the pandas DF of all the possible pokemon available to the class
        # statically, so we can reference it
        # df = pd.read_csv("data/pokemon.csv", index_col=None)

        # use the pokedex number to collect the pokemon information, and assign the
        #   necessary items
        pokemon_info = df_pokemon.loc[df_pokemon['name'] == pokemon_name]

        # input validation to make sure a real pokemon is entered
        while pokemon_info.empty:
            pokemon_name = input("Enter a valid/existing pokemon name: ")
            pokemon_info = df_pokemon.loc[df_pokemon['name'] == pokemon_name]

        # basic pokemon information
        self.attack = pokemon_info['attack'].to_string(index=False)
        self.defense = pokemon_info['defense'].to_string(index=False)
        self.hp = pokemon_info['hp'].to_string(index=False)
        self.name = pokemon_name
        self.pokedex_number = pokemon_info['pokedex_number'].to_string(
            index=False)
        self.sp_attack = pokemon_info['sp_attack'].to_string(index=False)
        self.sp_defense = pokemon_info['sp_defense'].to_string(index=False)
        self.speed = pokemon_info['speed'].to_string(index=False)
        self.type1 = pokemon_info['type1'].to_string(index=False)
        self.type2 = pokemon_info['type2'].to_string(index=False)
        self.nickname = nickname

        # init moves array, and move count
        self.moves = {}
        self.move_count = 0

    # repr method to print out the pokemon in a way useful for getting a quick
    # glance at the pokemon
    def __repr__(self) -> str:
        # print names
        info = "Pokemon Name: " + self.name + "\n"
        info += "Nickname: " + self.nickname + "\n"

        # print pokedex number
        info += "Pokedex Number: " + str(self.pokedex_number) + "\n"

        # print types
        if self.type2 == "Nan":
            info += "Type 1: " + self.type1 + "\n"
            info += "Type 2: " + self.type2 + "\n"
        else:
            info += "Type: " + self.type1 + "\n"

        return info

    # box print prints a pokemon in a useful way for the box
    def box_print(self) -> str:
        return self.name

    def print_stats(self) -> str:
        # print stats
        info = "~~~~~ " + str(self.nickname) + "'s Stats ~~~~~\n"
        info += "Attack: " + str(self.attack) + "\n"
        info += "Defense: " + str(self.defense) + "\n"
        info += "Health Points: " + str(self.hp) + "\n"
        info += "Special Attack: " + str(self.sp_attack) + "\n"
        info += "Special Defense: " + str(self.sp_defense) + "\n"
        info += "Speed: " + str(self.speed) + "\n"

        return info

    def print_moves(self) -> str:
        if self.move_count == 0:
            return self.nickname + " does not know any moves!"
        else:
            # print moves
            index = 1
            info = "~~~~~~ " + self.nickname + "'s Moves ~~~~~~\n"
            for move in self.moves.keys():
                info += "Move " + str(index) + ": " + str(move) + "\n"
                index += 1

            return info

    def teach_move(self, move_name: str) -> bool:
        # check for max move count
        if self.move_count >= self.MAX_MOVES:
            print(self.nickname +
                  " already knows 4 moves! Remove a move to teach it another one.")
            return False

        # else, add a new move by the move name
        else:
            if move_name in self.moves.keys():
                print("You cannot teach a pokemon two of the same move!")
                return False

            move = Move(move_name)
            self.moves[move_name] = move
            self.move_count += 1

            return True

    def remove_move(self, move_name: str) -> bool:
        # check to make sure the move exists
        if move_name not in self.moves.keys():
            print("You cannot remove a move that " +
                  self.nickname + " does not know!")
            return False

        # else, pop the item from the dictionary
        else:
            self.moves.pop(move_name)
            self.move_count -= 1
            return True

    # Incorrect Implementation of teach move, does not actually add the move
    def teach_move_incorrect(self, move_name: str) -> None:
        move = Move(move_name)
        self.move_count += 1

    def change_name(self, new_name: str) -> None:
        self.nickname = new_name
