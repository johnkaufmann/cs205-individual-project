from typing import Dict, List, Tuple
from Box import Box
from Pokemon import Pokemon


class PokemonManagementSystem:
    def __init__(self, owner: str) -> None:
        self.owner = owner
        self.boxes: Dict[str, Box] = {}
        self.box_count = 0

    def add_box(self, box_name: str):
        if box_name in self.boxes.keys():
            print("A box with the name " + str(box_name) +
                  " already exists! Add a box with a different name.")
            return False
        box = Box(box_name)
        self.boxes[box_name] = box
        self.box_count += 1

    def remove_box(self, box_name: str):
        if box_name not in self.boxes.keys():
            print("You cannot remove a box that does not exist in the system.")
            return False
        if self.boxes[box_name].pokemon_count > 0:
            print("You cannot delete a box that has pokemon in it.")
            return False

        self.boxes.pop(box_name)
        self.box_count -= 1

    def get_box(self, box_name: str):
        if box_name in self.boxes.keys():
            return self.boxes[box_name]

        return "Box not found."

    def add_pokemon_to_system(self, pokemon_name, pokemon_nickname, box_name):
        pokemon = Pokemon(pokemon_name, pokemon_nickname)

        # TODO: ensure that this works correctly
        self.boxes[box_name].add_pokemon(pokemon)

    def find_pokemon(self, pokemon_nickname, pokemon_name):
        found_pokemon: list[Pokemon] = []
        for box in self.boxes.values():
            mons = box.find_pokemon(pokemon_name, pokemon_nickname)
            if mons is not False:
                if type(mons) == List:
                    for mon in mons:
                        found_pokemon.append((mon, box))
                else:
                    found_pokemon.append((mons, box))

        if len(found_pokemon) > 0:
            print("~~~~~ FOUND POKEMON: ~~~~~")
            for pokemon_data in found_pokemon:
                print(pokemon_data[0])

            return found_pokemon

    def move_pokemon(self, pokemon_nickname, pokemon_name, to_box_name, from_box_name):
        # collect pokemon using self find pokemon method
        from_box = self.get_box(from_box_name)
        to_box = self.get_box(to_box_name)

        pokemon = from_box.find_pokemon(pokemon_name, pokemon_nickname)

        if pokemon is None:
            print(
                "ERROR: COULD NOT MOVE POKEMON. Make sure the pokemon and both the to and from boxes exist.")
            return False

        to_box.add_pokemon(pokemon)
        from_box.remove_pokemon(pokemon_nickname)

    def __repr__(self) -> str:
        info = "~~~~~ " + self.owner + "'s PC: ~~~~~\n"
        for box in self.boxes.values():
            info += str(box)

        return info
