import unittest
import Pokemon
import Move
import Box
import PokemonManagementSystem


class TestMove(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.move = Move.Move("Fly")

    @classmethod
    def tearDownClass(cls):
        cls.pokemon = None

    def test_create_move(self):
        move = Move.Move("Flare Blitz")

        # make sure effect is correct, to ensure it is the correct move
        assert move.effect == "User receives recoil damage. May burn opponent."


class TestPokemon(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.pokemon = Pokemon.Pokemon("Charizard", "test_nickname")

    @classmethod
    def tearDownClass(cls):
        cls.pokemon = None

    def test_create_pokemon(self):
        pokemon = Pokemon.Pokemon("Charizard", "nickname")

        # make sure the pokedex number is correct
        assert pokemon.pokedex_number == "6"

        # no need to test if correct name is inputted, it will prompt for user to
        # put valid pokemon name in

    def test_box_print(self):
        assert self.pokemon.box_print() == "Charizard"

    def test_teach_move(self):
        pokemon = Pokemon.Pokemon("Charizard", "test")
        # add a move
        result = pokemon.teach_move("Fly")
        assert "Fly" in pokemon.moves.keys()
        assert result == True
        assert pokemon.move_count == 1

        # check duplicate moves
        result = pokemon.teach_move("Fly")
        assert result == False

        # add 3 other moves, to get to 4 moves
        assert pokemon.teach_move("Flame Wheel") == True
        assert pokemon.teach_move("Flare Blitz") == True
        assert pokemon.teach_move("Flamethrower") == True

        # add a fifth move, should not work
        result = pokemon.teach_move("Aerial Ace")
        assert "Aerial Ace" not in pokemon.moves.keys()
        assert result == False

    # Incorrect Implementation Test
    def test_teach_move_incorrect(self):
        pokemon = Pokemon.Pokemon("Charizard", "test")

        result = pokemon.teach_move_incorrect("Fly")
        assert "Fly" in pokemon.moves.keys()
        assert result == True
        assert pokemon.move_count == 1

        # check duplicate moves
        result = pokemon.teach_move("Fly")
        assert result == False

        # add 3 other moves, to get to 4 moves
        assert pokemon.teach_move("Flame Wheel") == True
        assert pokemon.teach_move("Flare Blitz") == True
        assert pokemon.teach_move("Flamethrower") == True

        # add a fifth move, should not work
        result = pokemon.teach_move("Aerial Ace")
        assert "Aerial Ace" not in pokemon.moves.keys()
        assert result == False

    def test_remove_move(self):
        pokemon = Pokemon.Pokemon("Charizard", "test")
        # try removing a move that exists, check to make sure it no longer exists
        pokemon.remove_move("Flamethrower")
        assert "Flamethrower" not in pokemon.moves.keys()
        assert pokemon.move_count == 0

        # try to remove a move that does not exist
        assert pokemon.remove_move("Flamethrower") == False

    def test_change_name(self):
        self.pokemon.change_name("new name")
        assert self.pokemon.nickname == "new name"


class TestBox(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.box = Box.Box("Fire Types")

    @classmethod
    def tearDownClass(cls):
        cls.box = None

    def test_create_box(self):
        box = Box.Box("Fire Types")
        assert box.name == "Fire Types"

    def test_add_pokemon(self):
        box = Box.Box("Fire")
        pokemon = Pokemon.Pokemon("Charizard", "nickname")
        box.add_pokemon(pokemon)

        assert box.pokemon_count == 1
        assert box.pokemons[0] == pokemon

    def test_remove_pokemon(self):
        box = Box.Box("Water")
        pokemon = Pokemon.Pokemon("Charizard", "charizard2")
        box.add_pokemon(pokemon)

        box.remove_pokemon("charizard2")

        # this is one because of the previously added pokemon
        assert box.pokemon_count == 0

        try:
            assert box.pokemons[1]
        except IndexError:
            pass

    def test_find_pokemon(self):
        box = Box.Box("Water Types")
        pokemon = Pokemon.Pokemon("Charizard", "charizard3")
        box.add_pokemon(pokemon)

        found_mon = box.find_pokemon("Charizard", "charizard3")

        assert found_mon == pokemon

    def test_find_multiple_pokemon(self):
        box = Box.Box("Fire Types")
        poke1 = pokemon = Pokemon.Pokemon("Charmander", "Charm")
        poke2 = pokemon = Pokemon.Pokemon("Charmeleon", "Charm")

        box.add_pokemon(poke1)
        box.add_pokemon(poke2)

        found_mons = box.find_pokemon("Charmander", "Charm")

        assert len(found_mons) > 1
        assert found_mons[0] == poke1
        assert found_mons[1] == poke2


class TestManagementSystem(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

    @classmethod
    def tearDownClass(cls):
        cls.pc = None

    def test_create_pc(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        assert pc.owner == "Clay"

    def test_add_box(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        # add the box
        pc.add_box("Fire Types")

        # create a test box for checking types
        box = Box.Box("water types")

        assert pc.box_count > 0
        assert type(pc.boxes["Fire Types"]) == type(box)

        result = pc.add_box("Fire Types")
        assert result == False

    def test_add_pokemon_to_system(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        # add the box
        pc.add_box("Fire Types")

        pc.add_pokemon_to_system("Charizard", "Chard", "Fire Types")

        assert pc.boxes["Fire Types"].pokemon_count > 0

    def test_remove_box(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        # add a box
        pc.add_box("Grass Types")

        # test case of box with no pokemon (should delete)
        pc.remove_box("Grass Types")
        assert pc.box_count == 0

        # test case of box with pokemon (should NOT delete)
        pc.add_box("Water Types")
        pc.add_pokemon_to_system("Charizard", "Chard", "Water Types")

        pc.remove_box("Water Types")
        assert pc.box_count == 1

    def test_get_box(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        # add the box
        pc.add_box("Fire Types")

        found_box = pc.get_box("Fire Types")

        assert found_box == pc.boxes["Fire Types"]

    def test_find_pokemon(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        pc.add_box("Water")

        pc.add_pokemon_to_system("Vaporeon", "Snake", "Water")

        pokemon = pc.find_pokemon("Snake", "Vaporeon")
        print(pokemon)

        assert pokemon[0][0] == pc.boxes["Water"].pokemons[0]

    def test_move_pokemon(self):
        pc = PokemonManagementSystem.PokemonManagementSystem("Clay")

        pc.add_box("Water")
        pc.add_box("Favorite Pokemon")

        pc.add_pokemon_to_system("Vaporeon", "Snake", "Water")

        pc.move_pokemon("Snake", "Vaporeon", "Favorite Pokemon", "Water")

        assert pc.boxes["Water"].pokemon_count == 0
        assert pc.boxes["Favorite Pokemon"].pokemon_count == 1
        assert pc.boxes["Water"].find_pokemon("Vaporeon", "Snake") == False

# TODO: ADD A BROKEN TEST


if __name__ == "__main__":
    unittest.main()
