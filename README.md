# CS 205 Individual Project
Making the Pokemon Management System. 
Install requirements with `pip3 install -r requirements.txt`
Run the main project by running `main.py`, this runs a few example commands in
the interface.
Test the project with `coverage run -m unittest test_library.py`
Get a coverage report with `coverage report`.