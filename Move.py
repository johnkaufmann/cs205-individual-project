import pandas as pd

global df_moves
df_moves = pd.read_csv("data/All_Moves.csv")


class Move:
    def __init__(self, move_name: str) -> None:

        move_info = df_moves.loc[df_moves['Name'] == move_name]

        while move_info.empty:
            move_name = input("Please enter a valid/existing move name: ")
            move_info = df_moves.loc[df_moves['Name'] == move_name]

        self.type = move_info['Type'].to_string(index=False)
        self.effect = move_info['Effect'].to_string(index=False)
        self.category = move_info['Category'].to_string(index=False)
        self.power = move_info['Power'].to_string(index=False)
        self.accuracy = move_info['Acc'].to_string(index=False)
        self.name = move_name

    def __repr__(self) -> str:
        return self.name
