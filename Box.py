from Pokemon import Pokemon


class Box:
    def __init__(self, name: str) -> None:
        self.name = name
        self.pokemons: list[Pokemon] = []
        self.pokemon_count: int = 0

    # add pokemon method walks through steps of adding a pokemon...
    def add_pokemon(self, pokemon: Pokemon) -> None:
        self.pokemons.append(pokemon)
        self.pokemon_count += 1

    def remove_pokemon(self, pokemon_nickname) -> None:
        # find the specific pokemon
        if self.pokemon_count > 0:
            for pokemon in self.pokemons:
                if pokemon.nickname == pokemon_nickname:
                    self.pokemons.remove(pokemon)
                    self.pokemon_count = self.pokemon_count - 1

    def find_pokemon(self, pokemon_name, pokemon_nickname):
        found_pokemon: list[Pokemon] = []

        for pokemon in self.pokemons:
            if pokemon.name == pokemon_name:
                found_pokemon.append(pokemon)
            elif pokemon.nickname == pokemon_nickname:
                found_pokemon.append(pokemon)

        if len(found_pokemon) == 1:
            return found_pokemon[0]
        elif len(found_pokemon) > 0:
            return found_pokemon
        else:
            return False

    def __repr__(self) -> str:
        # need to somehow print all pokemon and the box in a nice way
        info = "~~~~~~ POKEMON IN THE BOX " + self.name + " ~~~~~~\n"

        if len(self.pokemons) > 0:
            for pokemon in self.pokemons:
                info += str(pokemon) + "\n"
        else:
            info += "No pokemon found in box."
        return info
