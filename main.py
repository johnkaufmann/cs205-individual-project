# main game logic here
from PokemonManagementSystem import PokemonManagementSystem


def display_options():
    print("Enter 1 to search for a pokemon.")
    print("Enter 2 to add a box to the system.")
    print("Enter 3 to add a pokemon to the system")
    print("Type `q` to quit.")


def main():
    # create PC
    pc = PokemonManagementSystem("Red")

    # Add a box
    pc.add_box("Fire Types")

    # Add a couple pokemon to the system
    pc.add_pokemon_to_system("Charizard", "Zardo", "Fire Types")
    pc.add_pokemon_to_system("Blaziken", "Blaze", "Fire Types")

    # Teach a pokemon a move
    pc.boxes["Fire Types"].pokemons[1].teach_move("Flame Wheel")

    # Print the Fire Types Box
    print(pc.boxes["Fire Types"])

    # Printing the move that we taught a pokemon
    print("Print `Blaze`s moves:")
    print(pc.boxes["Fire Types"].pokemons[1].print_moves())

    # Add a new box and pokemon
    pc.add_box("Fighting Types")
    pc.add_pokemon_to_system("Torchic", "Monkey", "Fighting Types")

    # Print the new box
    print(pc.boxes["Fighting Types"])

    # Move a pokemon, and print out the box again
    print("Moving a pokemon to another box:")
    pc.move_pokemon("Blaze", "Blaziken", "Fighting Types", "Fire Types")
    print(pc.boxes["Fighting Types"])

    # Print the entire pc (testing the pc's __repr__ method)
    print("Printing entire PC")
    print(pc)

    # Finding a specific pokemon using the pc find_pokemon method
    print("Finding a specific pokemon:")

    # this method also returns the box the pokemon was in and the pokemon object
    # itself, did not need to
    # demonstrate that here
    pc.find_pokemon("Blaze", "Blaziken")


main()
